const blog = require('./blog')
const users = require('./users')
const auth = require('./auth')
const express = require('express')
const app = express()

app.use(blog)
app.use(users)
app.use(auth)

module.exports = app;