const express = require('express')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const users = require('../model/users')
const admins = require('../model/admin')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 587,
	secure: false,
	requireTLS: true,
	auth: {
		user: "mernblog@gmail.com",
		pass: "Abcd@123"
	}
})

const app = express();

// *Getting all users from Mongo database
app.post('/api/getusers', async (req, res) => {
	const allUsers = await users.find();
	res.json(allUsers);

})

// *Register new user to mongo Database
app.post('/api/newuser', async (req, res) => {


	// *checking for email id exists or not
	let emailExists = await users.findOne({email: (req.body.email).toLowerCase()});
	if(emailExists){
		// *Deleting user data which is registered but not verified his email
		if(!emailExists.verified)
			await users.deleteOne({ _id: ObjectId(emailExists._id) });
		else
			return res.json({success: false, emailExists: true, msg: "Email id already exists!"});
	}

	// *checking for mobile number is exists or not
	let mobileExists = await users.findOne({mo: req.body.mo});
	if(mobileExists){
		// *Deleting user data which is registered but not verified his email
		if(!mobileExists.verified)
			await users.deleteOne({ _id: ObjectId(mobileExists._id) });
		else
			return res.json({success: false, mobileExists: true, msg: "Mobile number already exists!"});
	}

	const salt = await bcrypt.genSalt();
	const hashPassword = await bcrypt.hash(req.body.pass, salt)
	const lowerEmail = (req.body.email).toLowerCase();
	const query = new users({
		_id: new ObjectId,
		fname: req.body.fname,
		lname: req.body.lname,
		mo: req.body.mo,
		email: lowerEmail,
		pass: hashPassword,
		verified: false
	});

	const token = jwt.sign({email: lowerEmail}, 'Abcd@123')
	var mailOptions = {
		from : "coder.husen@gmail.com",
		to: req.body.email,
		subject: "Activation link from Husain Lokhandwala",
		html: "<center><h1>Hello  " + req.body.fname + " " + req.body.lname + "</h1><h1>Greetings from Husain Lokhandwala</h1><br /><h2>Thank you for joining us,</h2><br /><h3>Click on verify to Activate your Account :) <br /><a href=`https://mern1.netlify.com/user/login/" + token + "`>verify</a></h3></center>"
	}
	transporter.sendMail(mailOptions, (err, info) => {
		if(err)
			return console.log(err);
		else
			return console.log(`Information :- ${info.response}`);
	})
	console.log('Email Sent Successfully!');

	query.save().then(result => {
        res.status(200).json({
        	success: true,
            message : "Email verification link sent!"
        })
    }).catch(err => console.log(err));
})

// *Activate user
app.post('/api/userverify', async (req, res) => {
	const { token } = req.body;

	try {
		const decoded = jwt.verify(token, 'Abcd@123');
		const result = await users.updateOne({email: decoded.email}, {
			$set : {
				verified: true
			}
		})
		res.status(200).json({success: true, msg:'verified Successfully'});
	}
	catch (err) {
		res.json({success: false,msg: 'Invalid Token', err: err});
	}

})

// *Login api
app.post('/api/login', async (req, res) => {
	const admin = await admins.findOne({email: req.body.email});
	// if Admin found
	if(admin){
		const isMatch = await bcrypt.compare(req.body.pass, admin.pass);
		if(isMatch){
			const token = jwt.sign({id: admin._id}, 'Abcd@123')
			return res.json({
				token,
				type: "admin",
				user: {
					_id: admin._id,
					name: admin.name,
					email: admin.email,
				},
				success: true
			})
		}
		else
			return res.json({success: false, msg: "Invalid Credentials!"});
	}

	const user = await users.findOne({$or: [{email: (req.body.email).toLowerCase()}, {mo: req.body.email}]});
	if(!user)
		return res.json({emailNotExists: true, success: false, msg: "Email id or mobile number does not exists"});
	const isMatch = await bcrypt.compare(req.body.pass, user.pass);
	if(!isMatch)
		return res.json({success: false, msg: "Invalid Credentials!"});

	if(!user.verified)
		return res.json({isNotVerified: true, success: true, msg: "User is not verified!"})

	const token = jwt.sign({id: user._id}, 'Abcd@123')
	res.json({
		token,
		user: {
			_id: user._id,
			name: user.fname,
			lname: user.lname,
			email: user.email,
			mo: user.mo,
		},
		success: true
	})
})

// *Decode token and get data
app.post('/api/decodetoken', async (req, res) => {
	const { token } = req.body;
	try {
		const decoded = jwt.verify(token, 'Abcd@123');
		if(decoded.email)
			res.status(200).json({ email: decoded.email, success: true });
		else if(decoded.id)
			res.status(200).json({ id: decoded.id, success: true });
	}
	catch (err) {
		res.json({success: false,msg: 'Invalid Token', err: err});
	}

})

// *Forgot Password sendMail code
app.post('/api/forgotpass', async (req, res) => {
	const user = await users.findOne({email: (req.body.email).toLowerCase()});
	if(!user)
		return res.json({emailNotExists: true, success: false, msg: "Email id does not exists"});

	const token = jwt.sign({id: user._id}, 'Abcd@123')

	// if email exist then sending reset link to email

	var mailOptions = {
		from : "coder.husen@gmail.com",
		to: req.body.email,
		subject: "Reset Password Link!",
		html: "Click on verify to reset your password!<br /><a href=`https://mern1.netlify.com/user/user/resetpassword/" + token + "`>verify</a>"
	}
	transporter.sendMail(mailOptions, (err, info) => {
		if(err)
			return console.log(err);
		else
			return console.log(`Information :- ${info.response}`);
	})
	console.log('Email Sent Successfully!');
	res.json({
		success: true
	})
})

// *Reset password
app.post('/api/resetpass', async (req, res) => {

	const { token, pass } = req.body;
	if(token && pass){
		const salt = await bcrypt.genSalt();
		const hashPassword = await bcrypt.hash(pass, salt)
		const decoded = jwt.verify(token, 'Abcd@123');
		const result = await users.updateOne({_id: ObjectId(decoded.id)}, {
			$set : {
				pass: hashPassword,
				verified: true
			}
		})
		res.status(200).json({success: true, msg:'password has been changed'});
	}
	else{
		res.status(400).json({msg: 'Token and password is null'});
	}

})

// *Getting specific user by id for update prefilled
app.get('/api/user/:id', async (req, res) => {
	const user = await users.find({_id: ObjectId(req.params.id)});
	res.json(user);
})

// Change User verification status
app.post('/api/changeverified', async (req, res) => {
	const result = await users.updateOne({_id: ObjectId(req.body.id)}, {
		$set : {
			verified: req.body.status
		}
	})
	res.json({success: true});
})

// *Updating User Data
app.put('/api/updateuser/:id', async (req, res) => {
	const result = await users.updateOne({_id: ObjectId(req.params.id)}, {
		$set : {
			fname: req.body.fname,
			lname: req.body.lname,
			mo: req.body.mo,
			email: req.body.email,
			pass: req.body.pass
		}
	})
	res.status(200).json({
		message : "User Updated Successfully :)"
	})
})

// *Delete Blog by Id
app.delete('/api/deleteuser/:id', async (req, res) => {
	const result = await users.deleteOne({ _id: ObjectId(req.params.id) });
    res.status(200).json({
        message : "User Deleted!"
    })
})

module.exports = app;