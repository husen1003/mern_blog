const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const express = require('express')
const users = require('../model/users')
const app = express()

app.post('/api/auth', (req, res) => {
	const { token } = req.body;
	if(token) {
		try {
			const decoded = jwt.verify(token, "Abcd@123");
			req.user = decoded;

			users.findById(req.user.id)
				.then(user => {
					jwt.sign(
						{id: user._id}, "Abcd@123", (err, token) => {
							if(err) throw err;
							res
							.status(200)
							.json({
								success: true,
								token,
								msg: "Profile authenticated successfully!",
								user: {
									_id: user._id,
									name: user.fname,
									lname: user.lname,
									mo: user.mo,
									email: user.email
								}
							})
						}
					)
				})
				.catch(err => {
					res.json({error: err});
				})
		}
		catch(err) {
			res.status(200).json({success: false, msg: "Token not valid"});
		}
	}
	else {
		res.status(200).json({success: false, msg: "there is no token!"});
	}
})

module.exports = app;