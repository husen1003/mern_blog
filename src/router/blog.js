const express = require('express')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const blog = require('../model/blog')
const cors = require('cors');

const app = express();


// *Getting all blogs from Mongo database
app.get('/api/getblogs', async (req, res) => {
	const blogs = await blog.find();
	res.json(blogs);

})

// *Adding new blog to mongo Database
app.post('/api/newblog', async (req, res) => {
	const query = new blog({
		_id: new mongoose.Types.ObjectId,
		title: req.body.title,
		desc: req.body.desc
	});
	query.save().then(result => {
        console.log(result);
        res.status(201).json({
            message : "Blog Created"
        })
    }).catch(err => console.log(err));
})

// *Getting specific blog by id for update prefilled
app.get('/api/blog/:id', async (req, res) => {
	const blogid = await blog.find({_id: ObjectId(req.params.id)});
	res.json(blogid);
})

// *Updating Data
app.put('/api/updateblog/:id', async (req, res) => {
	const result = await blog.updateOne({_id: ObjectId(req.params.id)}, {
		$set : {
			title: req.body.title,
			desc: req.body.desc
		}
	})
	res.status(200).json({
		message : "Updated Successfully :)"
	})
})

// *Delete Blog by Id
app.delete('/api/deleteblog/:id', async (req, res) => {
	const result = await blog.deleteOne({ _id: ObjectId(req.params.id) });
    res.status(201).json({
        message : "Blog Deleted!"
    })
})

module.exports = app;