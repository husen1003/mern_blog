const mongoose = require('mongoose');

const blogSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	title: {type: String, required: true},
	desc: {type: String, required: true}
})

module.exports = mongoose.model('blog', blogSchema);