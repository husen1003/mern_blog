const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	fname: {type: String, required: true},
	lname: {type: String, required: true},
	mo: {type: String, required: true},
	email: {type: String, required: true, unique: true},
	pass: {type: String, required: true},
	verified: {type: Boolean, required: true}

})

module.exports = mongoose.model('users', userSchema)